@extends('layouts.app')


@section('content')
    <h1>create article</h1>
    <div class="container">
        <form enctype="multipart/form-data" method="POST" action="{{route('articles.store')}}">
            @csrf
           <div class="form-group">
               <label for="title">title</label>
               <input type="text" class="form-control" id="title" name="title">
           </div>
            <div class="form-group">
                <label for="description">description</label>
                <input type="text" class="form-control" id="description" name="description">
            </div>
            <button type="submit">submit</button>
        </form>
    </div>
@endsection
