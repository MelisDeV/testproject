@extends('layouts.app')



@section('content')

<div class="container">
    <h1>Articles</h1>
        <a href="{{route('articles.create')}}"><h1>Create new article</h1></a>
    <div class="row">
        @foreach($articles as $article)
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            {{$article->title}}
                        </h5>
                        <p class="card-text">author: <b>{{$article->user->name}}</b></p>
                        <a href="{{route('articles.show', ['article' => $article])}}"
                           class="btn btn-primary">
                            Show more</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div>
        {{$articles->links('pagination::bootstrap-4')}}
    </div>
</div>

@endsection
