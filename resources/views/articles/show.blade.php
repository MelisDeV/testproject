@extends('layouts.app')


@section('content')
    <div class="container">
        <h1>{{$article->title}}</h1>
        <h6>{{$article->user->name}}</h6>
        <p>{{$article->description}}</p>
    </div>
@endsection
